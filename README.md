# CoffeeTickets

## A simple system for assignment management.

## ( With intention to grow and add functionality and features in future :D )

Created by Viktor Melnyk

---

### Technologies used :

+ [Python](https://www.python.org/) 3.x because of the power and constantly growing number of cool features of this awesome programming language.
+ [Pip - Python package installer](https://pip.pypa.io/en/stable/) as default and most handy package manager for python.
+ [Django](https://www.djangoproject.com/) 2.x as easy to use Python framework for developing modular web applications.
+ Elements of [Django REST Framework](https://www.django-rest-framework.org/) 3.9.2
+ [Mypy - static type checker for Python](http://www.mypy-lang.org/) - a tool that lets you use Python as static typed language.
+ [Postgresql](https://www.postgresql.org/) DB engine, due to its advanced and handy features and optimization.
+ HTML5
+ CSS
+ JQuery as handy addition to plain JS
+ [Bootstrap4](https://getbootstrap.com/) as handy FrontEnd framework

### Internal Modules :

#### "ct" module contains :

settings - A Python file that contains Django settings like DB connection, static files and media routing, IP setings, DEBUG settings, installed apps, etc...

urls - A Python file that contains high level routing (endpoints/urls) and connects them to view functions of installed apps ot imports urls Python files of this apps for routing one level lower...

wsgi - A Python file used by WSGI development server

This module also contains base HTML template that later gets extended by other templates of app or apps, templates used for registration, password recovery, password change, etc...

#### "TicketsApp" module contains :

migrations - A folder of auto-generated Python files used for database setup and management of tables.

admin - A Python file that defines DB models that are accessible via admin interface.

apps - A small Python file that defines current app.

forms - A Python file that contains schemas of web forms for creation of Components, Projects, Tickets.

models - An important Python file that defines ORM models for DB interactions, creations and management of tables, and are used as interfaced data taken from DB and interfaced in Python. It contains:

+ Component model that only contains a name of a component
+ Project model that contains data about Project, its tasks, etc...
+ Ticket model that holds information about assignment, users involved, Project influenced, etc...

urls - A Python file that contains definitions of endpoints for routing them to view functions and classes.

views - A python file that contains functionality that processes requests and based on that produces responses that are then served.

+ index view of TicketsApp
+ create views for Component, Project, Ticket, utilizing forms
+ details views for Ticket objects and Project objects
+ utility view for Ticket status change

This module contains templates for index page of TicketsApp, detail views of Projects and Tickets, creation of Components, Projects, Tickets.

#### Additional files :

manage - A Python file that is a "command interface" for our Django project.

mypy - An initialization/config file for Mypy Python package/extention.

requirements - A list of required Python packages that are required to start our project. To install them, with Existing python 3.x instance we have to:

    $ python -m pip install -U pip
    $ cd "path_to_directory_with_project"
    $ pip instal -r requirements.txt

#### To start (Linux instructions):

Set up a Postgresql instance on local host (localhost), with port 5432.

After that...

    $ sudo su - postgres

...to log in as "postgres" user on your linux machine.

Then use...

    $ psql

...command to start PSQL shell.

Here we have to setup our user and DB.

    CREATE DATABASE ctdb;
    CREATE USER ct_user WITH PASSWORD 'T#f.2C4G8b2%Y7W]KXh8';
    ALTER ROLE ct_user SET client_encoding TO 'utf8';
    ALTER ROLE ct_user SET default_transaction_isolation TO 'read committed';
    ALTER ROLE ct_user SET timezone TO 'UTC';
    GRANT ALL PRIVILEGES ON DATABASE ctdb TO ct_user;

We can exit this shell with...
    
    \q

And exit postgres user with...

    $ exit

IMPORTANT NOTE

If we are getting error about connection and if instance of postgresql is running on REQUIRED in project settings port (default: 5432), problem may be solved by...

    $ sudo systemctl start postgresql.service

In project directory...

After we are sure that Python of a correct version is installed, Postgresql server is running and configure with right db to connect with options written down in config file...

    $ python manage.py makemigrations
    $ python manage,py migrate
    $ python manage.py createsuperuser
    $ python manage.py runserver

After this process we can see that server started locally, we can access it on http://127.0.0.1/

Log in with credentials of admin user, we've just created and create new Users, Projects, Tasks for our Users, etc...