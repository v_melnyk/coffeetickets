"""ct URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from django.contrib.auth import views as auth_views
from django.contrib.auth import login, logout
from django.views.generic.base import RedirectView


urlpatterns = [
    # LOGIN/LOGOUT
    path('accounts/login/', auth_views.LoginView.as_view(),
        {'template_name': "registration/login.html"}, name='login'),

    path('accounts/logout/', auth_views.LogoutView.as_view(), name='logout'),

    # PASSWORD RESET
    path('accounts/password_reset/', auth_views.PasswordResetView.as_view(),
        {'template_name': "registration/password_reset_form.html"}, name='password_reset'),

    path('accounts/password_reset/done/', auth_views.PasswordResetDoneView.as_view(),
        {'template_name': "registration/password_reset_done.html"}, name='password_reset_done'),

    re_path('^accounts/reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
            auth_views.PasswordResetConfirmView.as_view(),
            {'template_name': "registration/password_reset_confirm.html"},
            name='password_reset_confirm'),

    path('accounts/password_reset/done/', auth_views.PasswordResetCompleteView.as_view(),
        {'template_name': "registration/password_reset_complete.html"}, name='password_reset_complete'),

    # PASSWORD CHANGE
    path('accounts/password_change/',
        auth_views.PasswordChangeView.as_view(
            template_name='registration/psassword_change.html'),
        name='password_change'),

    path('accounts/password_change/done/',
        RedirectView.as_view(pattern_name='logout'),
        name='password_change_done'),

    # urls for TicketsApp
    path('', include('TicketsApp.urls')),

    # adding doc path
    # path('admin/doc/', include('django.contrib.admindocs.urls')),

    # admin urls included with django
    path('admin/', admin.site.urls),
]
