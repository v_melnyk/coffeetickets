from django.contrib import admin
from TicketsApp import models


# Adding models to an admin panel for administrative purposes
admin.site.register(models.Component)
admin.site.register(models.Project)
admin.site.register(models.Ticket)
