"""
Definition of Views for TicketsApp module...
"""
from typing import Type
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, HttpResponse, get_object_or_404
from django.http import HttpRequest
from django.db.models import Sum
from django.db.models.query import QuerySet
from TicketsApp import models
from TicketsApp import forms


@login_required
def index(request: HttpRequest) -> HttpResponse:
    """
    Index page view
    A board of tasks and projects
    """
    tickets: QuerySet = models.Ticket.objects.filter(
        assignee_id=request.user.id
    ).exclude(status__in=['STARTED', 'REJECTED']).order_by('-id')[:50]
    projects: QuerySet = models.Project.objects.filter(
        tickets__assignee_id=request.user.id
    ).distinct().order_by('-id')[:50]

    current_tickets: QuerySet = models.Ticket.objects.filter(
        assignee_id=request.user.id
    ).exclude(status__in=['DONE', 'REJECTED']).order_by('-id')[:50]

    context: dict = {
        'tickets': tickets,
        'projects': projects,
        'current_tickets': current_tickets
    }
    return render(request, 'TicketsApp/index.html', context)


@login_required
def component_create(request: HttpRequest) -> HttpResponse:
    """
    Helper view to create new components
    """
    if request.method == 'POST':
        form: forms.ComponentForm = forms.ComponentForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
    form_o: Type[forms.ComponentForm] = forms.ComponentForm
    return render(request, 'TicketsApp/component_form.html', {'form': form_o})


@login_required
def project_create(request: HttpRequest) -> HttpResponse:
    """
    View function to create new project
    """
    if request.method == 'POST':
        form: forms.ProjectForm = forms.ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            # form_o = forms.ProjectForm
            # out = render(request, 'tickets/project_form.html', {'form': form_o})
            return redirect('/')
    form_o: Type[forms.ProjectForm] = forms.ProjectForm
    return render(request, 'TicketsApp/project_form.html', {'form': form_o})


@login_required
def ticket_create(request: HttpRequest) -> HttpResponse:
    """
    View function to create new ticket
    """
    if request.method == 'POST':
        form: forms.TicketForm = forms.TicketForm(request.POST)
        if form.is_valid():
            form.save()
            # form_o = forms.TicketForm
            # out = render(request, 'tickets/ticket_form.html', {'form': form_o})
            return redirect('/')
    form_o: Type[forms.TicketForm] = forms.TicketForm
    return render(request, 'TicketsApp/ticket_form.html', {'form': form_o})


@login_required
def ticket_details(request: HttpRequest, t_id: int) -> HttpResponse:
    """
    View function to see ticket details
    """
    ticket: models.Ticket = get_object_or_404(models.Ticket, pk=t_id)
    components: str = ', '.join([t.name for t in ticket.components.all()])
    context: dict = {
        'ticket': ticket,
        'components': components
    }
    return render(request, 'TicketsApp/ticket_details.html', context)


@login_required
def ticket_status_change(request: HttpRequest, t_id: int, status: str) -> HttpResponse:
    """
    Helper view function to change status of ticket
    """
    ticket: models.Ticket = get_object_or_404(models.Ticket, pk=t_id)
    if (status, status) not in forms.CHOICES:
        return HttpResponse(status=400)
    ticket.status = status
    ticket.save()
    return redirect('/')


@login_required
def project_details(request: HttpRequest, p_id: int) -> HttpResponse:
    """
    View function to see Project details with list of related Tickets
    """
    project: models.Project = get_object_or_404(models.Project, pk=p_id)
    components: str = ', '.join([t.name for t in project.components.all()])
    supervisors: str = ', '.join([t.username for t in project.supervisors.all()])
    tickets: QuerySet = models.Ticket.objects.filter(
        project=project
    ).exclude(status='REJECTED')
    earned: int = tickets.aggregate(Sum('coffee_points'))['coffee_points__sum']
    tickets = tickets.order_by('-id')[:50]
    context: dict = {
        'project': project,
        'components': components,
        'supervisors': supervisors,
        'earned': earned,
        'tickets': tickets
    }
    return render(request, 'TicketsApp/project_details.html', context)
