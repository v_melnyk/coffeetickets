"""
Forms for models of TicketsApp module...
"""
from typing import Type
from django.forms import ModelForm, ChoiceField
from TicketsApp import models


CHOICES = (
    ('STARTED', 'STARTED'),
    ('REJECTED', 'REJECTED'),
    ('DONE', 'DONE')
)


class ComponentForm(ModelForm):
    """
    Form class for Component instance creation
    """
    class Meta:
        model: Type[models.Component] = models.Component
        fields: str = '__all__'


class ProjectForm(ModelForm):
    """
    Form class for Project instance creation
    """
    class Meta:
        model: Type[models.Project] = models.Project
        fields: str = '__all__'
        exclude: list = ['changelog']


class TicketForm(ModelForm):
    """
    Form class for Ticket instance creation
    """
    status: ChoiceField = ChoiceField(
        choices=CHOICES
    )

    class Meta:
        model: Type[models.Ticket] = models.Ticket
        fields: str = '__all__'
        exclude: list = ['comments', 'time_spent_hours', 'changelog']