"""
Definition of models (django ORM) for TicketsApp module...
"""
from django.db import models
from django.contrib.postgres.fields import JSONField
from django.contrib.auth.models import User


class Component(models.Model):
    """
    Utility class for project to store components
    """
    name: models.TextField = models.TextField()

    def __str__(self) -> str:
        """
        Helper utility function for string representation of a Component Class Object
        """
        return f'{self.id} - {self.name}'


class Project(models.Model):
    """
    Project class to gather tasks around one project
    """
    name: models.TextField = models.TextField()
    start_timestamp: models.DateTimeField = models.DateTimeField(auto_now=True)
    main_repo: models.URLField = models.URLField(blank=True)  # repo of a project if such exists
    components: models.ManyToManyField = models.ManyToManyField(  # components included in project
        Component,
        related_name='projects',
        blank=True
    )
    creator: models.ForeignKey = models.ForeignKey(
        User,
        related_name='projects_created',
        on_delete=models.DO_NOTHING,
        blank=True
    )
    supervisors: models.ManyToManyField = models.ManyToManyField(
        User,
        related_name='supervised',
        blank=True
    )
    changelog: JSONField = JSONField(blank=True, null=True)  # utility field to store history of changes to this model

    def __str__(self) -> str:
        """
        Helper utility function for string representation of a Project Class Object
        """
        return f'{self.id} - {self.name}({self.start_timestamp.strftime("%d %b %Y")})'


class Ticket(models.Model):
    """
    Tickets to be done assigned to user and connected to some project
    Components affected can be also stated
    """
    project: models.ForeignKey = models.ForeignKey(
        Project,
        on_delete=models.CASCADE,
        related_name='tickets',
        blank=True,
        null=True
    )
    coffee_points: models.IntegerField = models.IntegerField(default=0)
    summary: models.TextField = models.TextField()  # short name/description for ticket
    description: models.TextField = models.TextField()  # description of what has to be done in ticket
    creator: models.ForeignKey = models.ForeignKey(
        User,
        related_name='tickets_created',
        on_delete=models.DO_NOTHING,
        blank=True
    )
    assignee: models.ForeignKey = models.ForeignKey(  # user who was assigned to this ticket
        User,
        related_name='tickets_assigned',
        on_delete=models.DO_NOTHING,
        blank=True,
        null=True
    )

    components: models.ManyToManyField = models.ManyToManyField(  # components affected by this ticket
        Component,
        related_name='tickets',
        blank=True
    )
    status: models.TextField = models.TextField(default='STARTED')  # CURRENT status of the ticket
    comments: JSONField = JSONField(blank=True, null=True)  # comments to this ticket as Json with all additional info on comment
    changelog: JSONField = JSONField(blank=True, null=True)  # utility field to store history of changes to this model
    # included_media = JSONField(blank=True, null=True)  # pathes to media files represented as json
    # time_spent_hours = models.IntegerField(blank=True, null=True)

    def __str__(self) -> str:
        """
        Helper utility function for string representation of a Ticket Class Object
        """
        return f'{self.id} - {self.summary}'
