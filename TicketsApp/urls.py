from django.urls import path
from django.contrib.auth.decorators import login_required
from TicketsApp import views


urlpatterns: list = [
    # index view
    path('', views.index, name='index'),

    # creation forms ciews
    path('new/component', views.component_create, name='create_component'),
    path('new/project', views.project_create, name='create_project'),
    path('new/ticket', views.ticket_create, name='create_ticket'),

    # details views
    path('ticket/<int:t_id>', views.ticket_details, name='ticket_details'),
    path('project/<int:p_id>', views.project_details, name='project_details'),

    # utility views
    path('ticket/<int:t_id>/<str:status>', views.ticket_status_change, name='ticket_status_change'),
]
